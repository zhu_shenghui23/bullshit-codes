/**
 * 事情的起因: 
 * 同事做了一个商城, 马上就要上线了, 客户说还需要增加一个, 提醒商家发货的功能,然后就有了下面一段代码
 * 然后过了几天,客户问: 为什么点击这个提醒商家的功能没什么用呢
 * 我心里想: 这要是有用才是见了鬼了呢
 */
// 页面按钮
// <button id="reminder">提醒商家</button>

// 获取按钮元素  
var reminder = document.getElementById("reminder");  
  
// 添加点击事件监听器  
reminder.addEventListener("click", function() {  
  // 弹窗提示  
  alert("已提醒商家,尽快为您发货！");  
});