//
//  🐔💩.swift
//

import Foundation

class 🐔💩<🐔: Hashable, 💩> {
    private var 💩🪣 = [🐔: 💩]()
    
    func 👈(_ 🐔: 🐔, 💩📦: @escaping ()-> 💩) -> 💩 {
        if 💩🪣[🐔] == nil {
            💩🪣[🐔] = 💩📦()
        }
        return 💩🪣[🐔]!
    }
    
    func 👈(_ 🐔: 🐔) -> 💩? {
        return 💩🪣[🐔]
    }
    
    func 👉(_ 🐔: 🐔, _ 💩: 💩) -> 💩 {
        💩🪣[🐔] = 💩
        return 💩
    }
    
    func 🔄(_ 🐔: 🐔) {
        💩🪣.removeValue(forKey: 🐔)
    }
    
    func 🔄() {
        💩🪣.removeAll()
    }
}