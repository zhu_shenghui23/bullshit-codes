// 接到某产品代码重构的任务，当我看到下面的方法的时候，我以为没有被调用，问题不大，我擦！
// 结果被调用的地方还特别多，方法参数都没被使用且只是在方法里new了个对象，方法参数的价值已经为0，
// 这就算了，看到JSONObject了吧？你以为提供类的依赖来自jackson？fastjson？gson？没想到吧？
// 居然是自定义的，于是我又排查了一波，还有个叫JSONArray的类，绝了，关键是这两自定义对象容易引起误解
// 不说，这两个类的代码，也是封装的毫无逻辑，乱七八糟，这里就不贴代码了，然而这只是整个产品代码的冰山
// 一角，不过不得不服气的是，居然出到了6.0.0的版本了，还是牛逼，必须说牛逼一波！
// 代码来源于公司一位计算机毕业的研究生，工作几年的java大神写的，膜拜一波！
// 看完整个产品需要代码重构后，我心态已经炸裂
/**
 * @author Administrator
 */
public class ResultCodec {

    public static JSONObject encode(Result result){
        JSONObject jsonObject = new JSONObject();
        return jsonObject;
    }

    public static Result decode(JSONObject jsonObject){
        Result result = new Result();
        return result;
    }
}