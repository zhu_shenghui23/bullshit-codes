#include <iostream>
#include <vector>
using namespace std;

struct A {
    vector<int> v;
};

int main() {
    // Init the struct with 'malloc' which has a vector
    A* a = reinterpret_cast<A*>(malloc(sizeof(A)));
    // unexpected action
    a->v.push_back(100);
    return 0;
}