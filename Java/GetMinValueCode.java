import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 获取产品长宽高中最小值
 */
public class GetMinValueCode {
    public BigDecimal getProductMinSize(TbProduct product){
        BigDecimal productLength = new BigDecimal(StringUtils.isEmpty(product.getProductLength()) ? "0":product.getProductLength());
        BigDecimal productWidth = new BigDecimal(StringUtils.isEmpty(product.getProductWidth()) ? "0":product.getProductWidth());
        BigDecimal productHeight = new BigDecimal(StringUtils.isEmpty(product.getProductHeight()) ? "0":product.getProductHeight());
        BigDecimal productMaxSize = productLength.compareTo(productWidth) == -1 ? productLength:productWidth;
        return productMaxSize.compareTo(productHeight) == -1 ? productMaxSize:productHeight;
    }
}
